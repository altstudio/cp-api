const multer = require('multer');
const path = require('path');
const mongoose = require('mongoose');

const { Attachment } = require('../../models');
const mimeTypes = require('../../config/mimeTypes');
const PathToUpload = 'public/uploads/';

const storage = multer.diskStorage({
    destination (req, file, cb) {
        cb(null, PathToUpload);
    },
    async filename (req, file, cb) {
        
        try {
            const _id = new mongoose.Types.ObjectId();
            const attach = new Attachment({
                _id,
                link: 'uploads/'+_id.toString()+path.extname(file.originalname),
                name: file.originalname,
                mimetype: file.mimetype,
                path: PathToUpload
            });
            await attach.save();

            req.attachments =  [...(req.attachments ? req.attachments : []), attach];

            let ext = path.extname(file.originalname);
            if (!ext) {
                ext = file.mimetype.replace('image/', '.');
            }
            const filename = _id + ext;

            cb(null, filename);
        } catch (err) {
            console.log(err);
            cb(err, null);
        }
    }
});

const fileFilter = (req, file, cb) => {
    let types = '';
    types = types + mimeTypes.microsoftOffice.join() 
    types = types + mimeTypes.libreOffice.join() 
    types = types + mimeTypes.images.join() 
    types = types + mimeTypes.video.join() 
    types = types + mimeTypes.audio.join() 
    types = types + mimeTypes.other.join();

    if (!types.includes(file.mimetype)) {
        let err = new Error(`File type "${file.mimetype}" is forbidden.`);
        console.log(err);
        // TODO: обработка этой ошибки везде, где используется мультер
        return cb(null, false, err)
    }
  
    cb(null, true)
};

module.exports = multer({ storage, fileFilter });