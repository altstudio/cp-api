const User = require('../../models/User');

module.exports.register = async (req, res, next) => {
    req.checkBody({
        email: {
            in: ['body'],
            exists: {
                errorMessage: 'Email is required'
            },
            custom: {
                options: (value) => {
                    return new Promise((resolve, reject) => {
                        User.findOne({email: value.toLowerCase()})
                            .then(doc => {
                                if (!doc) {
                                    resolve();
                                } else {
                                    reject();
                                }
                            }, err => {
                                reject();
                            });
                    });
                },
                errorMessage: 'This email is already taken'
            },                    
            customSanitizer: {
                options: (value) => {
                    return value.toLowerCase();
                }
            },
            isEmail: {
                errorMessage: 'Invalid email'
            }
        },
        password: {
            in: ['body'],
            exists: {
                errorMessage: 'Password is required'
            },
            isLength: {
                options: {
                    min: 6
                },
                errorMessage: 'Passwords must be at least 6 chars long'
            }
        },
        passwordConfirmation: {
            in: ['body'],
            exists: {
                errorMessage: 'Password confirmation is required'
            },
            custom: {
                options: (value) => {
                    if (value !== req.body.password) {
                        return Promise.reject();
                    }
                    return Promise.resolve();
                },
                errorMessage: 'Password confirmation does not match password'
            }
        },
        fullname: {
            in: ['body'],
            notEmpty: {
                errorMessage: 'ФИО обязательно'
            },
            exists: {
                errorMessage: 'ФИО обязательно'
            }
        },
        mail: {
            in: ['body'],
            notEmpty: {
                errorMessage: 'Почтовый адрес обязателен'
            },
            exists: {
                errorMessage: 'Почтовый адрес обязателен'
            }
        }
    });
    const errors = await req.getValidationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({success: false, error: {errors: errors.mapped(), message: 'Bad request'}});
    }
    next();
};

module.exports.login = async (req, res, next) => {
    req.checkBody({
        email: {
            in: ['body'],
            exists: {
                errorMessage: 'Email is required'
            },
            isEmail: {
                errorMessage: 'Invalid email'
            },
            customSanitizer: {
                options: (value) => {
                    return value.toLowerCase()
                }
            }
        },
        password: {
            in: ['body'],
            notEmpty: {
                errorMessage: 'Password is required'
            },
            exists: {
                errorMessage: 'Password is required'
            }
        }
    });
    const errors = await req.getValidationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({success: false, error: {errors: errors.mapped(), message: 'Bad request'}});
    }
    next();
};

module.exports.profile = async (req, res, next) => {
    next();
};