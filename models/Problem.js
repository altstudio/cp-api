const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const config = require('../config/config');

const BaseModel = require('./BaseModel');
const Competition = require('./Competition');
const CompetitionCompleted = require('./CompetitionCompleted');

const Problem = BaseModel({
    description : {
        type: String,
        required: true
    },
    address: {
        type: String
    },
    location: [Number],
    attachments: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Attachment'
        }
    ],
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    comments: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Comment'
        }
    ],
    resultDescription: {
        type: String,
    },
    resultAttachments: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Attachment'
        }
    ],
    impressions: {
        type: Number,
        default: 0
    },
    points: {
        type: Number,
        default: 0
    },
    impressionsCompleted: {
        type: Number,
        default: 0
    },
    pointsCompleted: {
        type: Number,
        default: 0
    },
    status: {
        type: String,
        default: 'open' // open, declined, process, completed
    }
})

Problem.methods.findAlreadyComparedForUser = async function (user) {
    const competitions = await Competition.find({problems: {$elemMatch: {$eq: this._id}}, user: user});
    const result = [this._id];
    competitions.forEach(c => {
        c.problems.forEach(p => {
            if (p != this._id) {
                result.push(p);
            }
        })
    });
    return result;
}
Problem.methods.findAlreadyComparedForUserCompleted = async function (user) {
    const competitions = await CompetitionCompleted.find({problems: {$elemMatch: {$eq: this._id}}, user: user});
    const result = [this._id];
    competitions.forEach(c => {
        c.problems.forEach(p => {
            if (p != this._id) {
                result.push(p);
            }
        })
    });
    return result;
}

// индусский код 
// todo: переделать
Problem.statics.findRandomPair = async function (user) {
    const problems = await this.find({"status": "open"});
    let matrix = [];
    for (let i = 0; i < problems.length; i++) {
        matrix[problems[i]._id] = {};
        for (let j = 0; j < problems.length; j++) {
            if (i != j) {
                matrix[problems[i]._id][problems[j]._id] = 0;
            }
        }
    }
    const competitions = await Competition.find({user: user});
    for (let i = 0; i < competitions.length; i++) {
        let f = competitions[i].problems[0];
        let s = competitions[i].problems[1];
        if (matrix.hasOwnProperty(f)) {
            matrix[f][s] = 1;
        }
        if (matrix.hasOwnProperty(s)) {
            matrix[s][f] = 1;
        }
    }
    for (let i = 0; i < problems.length; i++) {
        for (let j = 0; j < problems.length; j++) {
            if (i != j && matrix[problems[i]._id][problems[j]._id] == 0) {
                return [problems[i], problems[j]];
            }
        }
    }
    return false;
}

// метод с не самой лучшей рейтинговой системой, но хоть как то (предполагаем что количество голосований намного больше чем количество добавляемых проблем)
// todo: вот здесь нужно будет дико апгрейдить рейтинговую систему и все такое
Problem.statics.findPair = async function (user) {
    const variant = await this.findOne({ "status": "open" }).populate('attachments', {link: 1}).sort({impressions: 1}); // выбираем с наименьшим количеством показов
    
    const alreadyCompared = await variant.findAlreadyComparedForUser(user);
   
    const allProblems = await this.find({ "_id": { "$nin": alreadyCompared }, "status": "open" }).populate('attachments', {link: 1});
    
    if (allProblems.length == 0) { // все плохо и с минимальными просмотрами истекли варианты (можно еще рандома добавить будет)
        let pair = await this.findRandomPair(user);
        if (pair) {
            await Promise.all(pair.map(i => {
                return new Promise(async function (resolve, reject) {
                    i.impressions++;
                    await i.save()
                    resolve();
                });
            }));
            
            return pair;
        }
        return [];
    }
    let minDiff = 9999;
    allProblems.forEach(p => {
        if (Math.abs(p.points - variant.points) < minDiff) {
            minDiff = Math.abs(p.points - variant.points);
        }
    });

    const problems = allProblems.filter(p => Math.abs(p.points - variant.points) == minDiff);

    const count = problems.length;
    const random = Math.floor(Math.random() * count);

    await Promise.all([variant, problems[random]].map(i => {
        return new Promise(async function (resolve, reject) {
            i.impressions++;
            await i.save()
            resolve();
        });
    }));
    
    return [variant, problems[random]];
}



// дубль todo: рефактор
Problem.statics.findRandomPairCompleted = async function (user) {
    const problems = await this.find({"status": "completed"});
    let matrix = [];
    for (let i = 0; i < problems.length; i++) {
        matrix[problems[i]._id] = {};
        for (let j = 0; j < problems.length; j++) {
            if (i != j) {
                matrix[problems[i]._id][problems[j]._id] = 0;
            }
        }
    }
    const competitions = await CompetitionCompleted.find({user: user});
    for (let i = 0; i < competitions.length; i++) {
        let f = competitions[i].problems[0];
        let s = competitions[i].problems[1];
        if (matrix.hasOwnProperty(f)) {
            matrix[f][s] = 1;
        }
        if (matrix.hasOwnProperty(s)) {
            matrix[s][f] = 1;
        }
    }
    for (let i = 0; i < problems.length; i++) {
        for (let j = 0; j < problems.length; j++) {
            if (i != j && matrix[problems[i]._id][problems[j]._id] == 0) {
                return [problems[i], problems[j]];
            }
        }
    }
    return false;
}

// метод с не самой лучшей рейтинговой системой, но хоть как то (предполагаем что количество голосований намного больше чем количество добавляемых проблем)
// todo: вот здесь нужно будет дико апгрейдить рейтинговую систему и все такое
Problem.statics.findPairCompleted = async function (user) {
    const variant = await this.findOne({ "status": "completed" }).populate('attachments', {link: 1}).sort({impressions: 1}); // выбираем с наименьшим количеством показов
    if (!variant) {
        return [];
    }
    const alreadyCompared = await variant.findAlreadyComparedForUserCompleted(user);
   
    const allProblems = await this.find({ "_id": { "$nin": alreadyCompared }, "status": "completed" }).populate('attachments', {link: 1}).populate('resultAttachments', {link: 1});
    
    if (allProblems.length == 0) { // все плохо и с минимальными просмотрами истекли варианты (можно еще рандома добавить будет)
        let pair = await this.findRandomPairCompleted(user);
        if (pair) {
            await Promise.all(pair.map(i => {
                return new Promise(async function (resolve, reject) {
                    i.impressionsCompleted++;
                    await i.save()
                    resolve();
                });
            }));
            
            return pair;
        }
        return [];
    }
    let minDiff = 9999;
    allProblems.forEach(p => {
        if (Math.abs(p.points - variant.points) < minDiff) {
            minDiff = Math.abs(p.points - variant.points);
        }
    });

    const problems = allProblems.filter(p => Math.abs(p.points - variant.points) == minDiff);

    const count = problems.length;
    const random = Math.floor(Math.random() * count);

    await Promise.all([variant, problems[random]].map(i => {
        return new Promise(async function (resolve, reject) {
            i.impressionsCompleted++;
            await i.save()
            resolve();
        });
    }));
    
    return [variant, problems[random]];
}


module.exports = mongoose.model('Problem', Problem);