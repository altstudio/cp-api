const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BaseModel = require('./BaseModel');

const Comment = BaseModel({
    sender: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },   
    text: {
        type: String
    },
    attachments: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Attachment'
        }
    ],
    type: {
        type: String,
        default: 'message' // default or system
    }
}, { usePushEach: true });

module.exports = mongoose.model('Comment', Comment);