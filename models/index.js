const path = require("path");
const fs = require("fs");

const models = {};

require("fs").readdirSync(path.join(__dirname, "./")).forEach(file => {
    if (file === 'index.js' || !fs.lstatSync(path.join(__dirname, file)).isFile() || path.extname(file) !== '.js') {
        return;
    }
    const name = file.replace('.js', '');
    models[name] = require(`./${file}`);
});

module.exports = models;