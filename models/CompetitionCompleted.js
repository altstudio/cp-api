const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BaseModel = require('./BaseModel');

const CompetitionCompleted = BaseModel({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },   
    decision: {
        type: Schema.Types.ObjectId,
        ref: 'Problem'
    },
    problems: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Problem'
        }
    ]
});

CompetitionCompleted.statics.getCurrentForUser = async function (user) {
    return await this.findOne({user: user, decision: {$exists: false}});
}

module.exports = mongoose.model('CompetitionCompleted', CompetitionCompleted);