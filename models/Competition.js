const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BaseModel = require('./BaseModel');

const Competition = BaseModel({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },   
    decision: {
        type: Schema.Types.ObjectId,
        ref: 'Problem'
    },
    problems: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Problem'
        }
    ]
});

Competition.statics.getCurrentForUser = async function (user) {
    return await this.findOne({user: user, decision: {$exists: false}});
}

module.exports = mongoose.model('Competition', Competition);