const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcrypt');
const config = require('../config/config');
const jwt = require('jsonwebtoken');

const BaseModel = require('./BaseModel');

const User = BaseModel({
    email : {
        type: String,
        required: true,
        unique: true
    },
    phone: {
        type: String
    },
    password : {
        type: String,
        required: true
    },
    fullname: {
        required: true,
        type: String
    },
    mail: {
        required: true,
        type: String
    },
    profileImg: {
        type: String
    },
    role: {
        type: String
    }
})

User.virtual('fullName').get(function () {
    return this.firstname + ' ' + this.lastname;
});

User.plugin(uniqueValidator, {message: 'This {PATH} already exists'});

User.methods.comparePassword = async function (password) {
    return await bcrypt.compareSync(password, this.password);
};

User.methods.generateJwt = function () {
    const payload = {
        _id: this._id,
        email: this.email
    };
    const token = jwt.sign(payload, config.secret, {
        expiresIn: config.tokenExpiresIn
    });
    return token;
};

User.pre('save', async function (next) {
    if (!this.isModified('password') || !this.password) return next();
    const salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this._doc.password, salt);
    next();
});

module.exports = mongoose.model('User', User);