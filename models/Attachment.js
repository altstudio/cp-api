const mongoose = require('mongoose');
const path = require('path');
const fs = require('fs');
const Schema = mongoose.Schema;

const BaseModel = require('./BaseModel');

const Attachment = BaseModel({
    name: {
        type: String
    },
    mimetype: {
        type: String
    },
    path: {
        type: String
    },
    link: {
        type: String
    }
});

Attachment.statics.generateLink = function (attachment) {
    let obj = {
        link: `${attachment.path}${attachment._id.toString()}${path.extname(attachment.name)}?token=${attachment.token}`,
        id: attachment._id,
        name: attachment.name,
        mime: attachment.mimetype
    }
    return obj;
}

Attachment.statics.checkToken = async function (filename, token) {
    const _id = filename.split('.')[0];
    const attachment = await this.findOne({ _id });
    return token === attachment.token;
}

Attachment.statics.createFromArrayBuffer = async function (file) {
    try {
        const { name, type, size, data } = file;

        // конвертим arrayBuffer в buffer
        const buffer = new Buffer(new Uint8Array(data));

        // _id аттачмента = имени файла в папке
        const _id = new mongoose.Types.ObjectId();
        const ext = path.extname(name);

        const filePath = `${path.join(__dirname, '../uploads/')}${_id}${ext}`;

        // пытаемся сохранить картинку
        await fs.writeFile(filePath, buffer, { flag: 'w' }, err => {
            if (err) {
                throw err;
            }
        });

        // создаем аттачмент
        const attachment = new this({
            _id,
            name,
            mimetype: type,
            path: 'uploads/'
        });

        await attachment.save();

        return attachment;
    } catch (err) {
        console.log('Create file from array buffer error', err);
    }
};

// why pre middleware is not works for remove - https://github.com/Automattic/mongoose/issues/5999#issuecomment-358412876
Attachment.statics.removeWithFiles = async function (query) {
    const attachments = await this.find(query);

    // need to remove files
    await Promise.all(attachments.map(att => fs.unlinkSync(`${path.join(__dirname, '../uploads/')}${att._id}${path.extname(att.name)}`)));
    // now remove attachments
    await this.deleteMany(query);
}

module.exports = mongoose.model('Attachment', Attachment);