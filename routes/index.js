const express = require('express');
const router = express.Router();

const { Problem, Competition } = require('../models');

router.get('/stats', async (req, res) => {
    try {
        const count = await Problem.find().countDocuments();
        const countProcess = await Problem.find({status: {$ne: 'open'}}).countDocuments();
        const completedCount = await Problem.find({status: 'completed'}).countDocuments();

        const usersVoted = await Competition.aggregate([
            {
                $match: {
                    decision: {
                        $exists: true
                    }
                }
            },
            {
                $group : {
                    _id : "$user",
                }
            }
        ]);
        const competitionsCount = await Competition.find({decision: {$exists: true}}).countDocuments();
        
        return res.json({
            count,
            countProcess,
            completedCount,
            usersVoted: usersVoted.length,
            competitionsCount
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({success: false, error: err});
    }
});

module.exports = router;