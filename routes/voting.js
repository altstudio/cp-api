const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const { Competition, Problem, CompetitionCompleted } = require('../models');

const passport = require('passport');
require('../config/passport.js')(passport);

// логика выдачи итемов на голосование (ща будут костыли)
router.get('/', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        // пара уже была создана, но решение не принято
        let pair = null;
        let competition = await Competition.getCurrentForUser(req.user._id);
        if (competition) {
            pair = await Problem.find({_id: {$in: competition.problems}}).populate('attachments', {link: 1});
            return res.json({pair: pair, competition: competition._id});
        }
        // нужно выбрать и создать новую пару
        pair = await Problem.findPair(req.user._id);
        if (pair.length == 0) {
            return res.json({pair: null});
        }
        competition = new Competition({
            _id: new mongoose.Types.ObjectId(),
            user: req.user._id,
            problems: pair.map(p => p._id),
        });
        await competition.save();

        return res.json({pair: pair, competition: competition._id});
    } catch (err) {
        console.log(err);
        return res.status(500).json({success: false, error: err});
    }
});



router.get('/completed', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        // пара уже была создана, но решение не принято
        let pair = null;
        let competition = await CompetitionCompleted.getCurrentForUser(req.user._id);
        if (competition) {
            pair = await Problem.find({_id: {$in: competition.problems}}).populate('attachments', {link: 1}).populate('resultAttachments', {link: 1});
            return res.json({pair: pair, competition: competition._id});
        }
        // нужно выбрать и создать новую пару
        pair = await Problem.findPairCompleted(req.user._id);
        if (pair.length == 0) {
            return res.json({pair: null});
        }
        competition = new CompetitionCompleted({
            _id: new mongoose.Types.ObjectId(),
            user: req.user._id,
            problems: pair.map(p => p._id),
        });
        await competition.save();

        return res.json({pair: pair, competition: competition._id});
    } catch (err) {
        console.log(err);
        return res.status(500).json({success: false, error: err});
    }
});

// прием голоса, по определенному компетитион
router.post('/', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const { competition, id } = req.body;
        // прибавление рейтинга
        const model = await Competition.findById(competition);
        const problem = await Problem.findOne({_id: id});
        if (!problem || !model) {
            return res.status(400).json({success: false});
        }
        problem.points++;
        await problem.save();
        model.decision = id;
        await model.save();
        return res.json({success: true});
    } catch (err) {
        console.log(err);
        return res.status(500).json({success: false, error: err});
    }
});

// прием голоса, по определенному компетитион
router.post('/completed', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const { competition, id } = req.body;
        // прибавление рейтинга
        const model = await CompetitionCompleted.findById(competition);
        const problem = await Problem.findOne({_id: id});
        if (!problem || !model) {
            return res.status(400).json({success: false});
        }
        problem.pointsCompleted++;
        await problem.save();
        model.decision = id;
        await model.save();
        return res.json({success: true});
    } catch (err) {
        console.log(err);
        return res.status(500).json({success: false, error: err});
    }
})

module.exports = router;