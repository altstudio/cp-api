const express = require('express');
const router = express.Router();

const { Problem, User } = require('../../models');
const upload = require('../../middlewares/upload/attachmentUpload');

const passport = require('passport');
require('../../config/passport.js')(passport);

/*
мозг уже не соображает, изменять заявки через бд - муторно, пилить нормальную админку тоже. говнокодю на автомате. todo: переделать полностью
*/
router.get('/login', async (req, response) => {
    try {
        response.writeHead(200, {
            'Content-Type': 'text/html; charset=utf-8',
        });
        response.write('<link rel="stylesheet" href="/assets/styles/bootstrap.min.css">');
        response.write('<div class="container"><h1 style="margin: 50px 0 20px;">Работа с обращениями граждан</h1></p>');
        response.write('<form method="POST">');
        response.write('<div class="form-group"><input class="form-control" type="text" name="email" placeholder="email" required></div>');
        response.write('<div class="form-group"><input class="form-control" type="password" name="password" placeholder="пароль" required></div>');
        response.write('<div class="form-group"><input class="btn btn-success" type="submit"></div>');
        response.write('</form>');
        return response.end();
    } catch (err) {
        return res.status(500).json({success: false, error: err});
    }
});
router.post('/logout', async (req, res) => {
    try {
        res.cookie('jwt_token', {expires: Date.now()});
        return res.redirect('/admin/login');
    } catch (err) {
        return res.redirect('/admin');
        // console.log(err);
        // return res.status(500).json({success: false, error: err});
    }
});
router.post('/login', async (req, res) => {
    try {
        const { email, password } = req.body;
        const user = await User.findOne({email: email});
        if (user && await user.comparePassword(password) && user.role === 'superadmin') {
            const token = user.generateJwt();
            res.cookie('jwt_token', token);
            return res.redirect('/admin');
        }
        return res.redirect('/admin/login');
    } catch (err) {
        console.log(err);
        return res.status(500).json({success: false, error: err});
    }
});

router.get('/', passport.authenticate('jwt', {session: false}), async (req, response) => {
    try {
        const problems = await Problem.find({"status": "open"});
        response.writeHead(200, {
            'Content-Type': 'text/html; charset=utf-8',
        });
        response.write('<link rel="stylesheet" href="/assets/styles/bootstrap.min.css">');
        response.write('<div class="container"><h1 style="margin: 50px 0 20px;">Открытые обращения граждан</h1><p><form method="POST" action="/admin/logout"><button type="submit" class="btn btn-info">Выйти</button></form></p>');
        problems.forEach(p => {
            response.write('<form method="POST" enctype="multipart/form-data">');
            response.write('<h4>'+p.address+'</h4>');
            response.write('<p>'+p.description+'</p>');
            response.write('<div class="form-group">Итоги работы или причина отказа:<br><textarea class="form-control" name="resultDescription"></textarea></div>');
            response.write('<div class="form-group"><input class="btn btn-info" type="file" name="attachments" required multiple></div>');
            response.write('<div class="form-group"><input class="btn btn-success" type="submit" name="completed" value="Проблема решена">'+"&nbsp;"+'<input class="btn btn-danger" type="submit" name="declined" value="Отклонить"></div>');
            response.write('<input type="hidden" name="id" value="'+p._id+'"></form><hr>');
        });        
        response.write('</div>');
        return response.end();
    } catch (err) {
        console.log(err);
        return res.status(500).json({success: false, error: err});
    }
});

router.post('/', passport.authenticate('jwt', {session: false}), upload.array('attachments'), async (req, res) => {
    try {
        const problem = await Problem.findById(req.body.id);
        problem.resultAttachments = req.attachments.map(a => a._id);
        problem.resultDescription = req.body.resultDescription;
        problem.status = req.body.completed ? 'completed' : 'declined';
        await problem.save();
        return res.redirect('/admin');
    } catch (err) {
        if (req.attachments)
            Attachment.removeWithFiles( queryFewIds( req.attachments ) );
        console.log(err);
        return res.status(500).json({success: false, error: err});
    }
});

module.exports = router;