const express = require('express');
const router = express.Router();
const User = require('../models/User');
const mongoose = require('mongoose');

const passport = require('passport');
require('../config/passport.js')(passport);

const validation = require('../middlewares/validation/user');

router.get('/', passport.authenticate('jwt', {session: false}), (req, res) => {
    return res.json(req.user);
});

router.post('/register', validation.register, async (req, res) => {
    try {
        const { email, password, fullname, mail } = req.body;
        const user = await new User({
            _id: new mongoose.Types.ObjectId(),
            email: email,
            password: password,
            fullname: fullname,
            mail: mail
        });
        await user.save();
        const token = user.generateJwt();
        return res.json({success: true, token: token});
    } catch (err) {
        return res.status(500).json({success: false, error: err});
    }
});

router.post('/login', validation.login, async (req, res) => {
    try {
        const { email, password } = req.body;
        const user = await User.findOne({email: email});
        if (user && await user.comparePassword(password)) {
            const token = user.generateJwt();
            return res.json({success: true, token: token});
        }
        return res.status(401).send({success: false, message: 'Authentication failed. Wrong email or password.'});
    } catch (err) {
        return res.status(500).json({success: false, error: err});
    }
});


// TODO: implement profile editing
router.post('/', validation.profile, (req, res) => {
    try {
        return res.json({});
    } catch (err) {
        return res.json({success: false, error: err});
    }
});

// TODO: implement VK login/registration

module.exports = router;