const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Problem = require('../models/Problem');

const passport = require('passport');
require('../config/passport.js')(passport);

const upload = require('../middlewares/upload/attachmentUpload');

router.get('/', async (req, res) => {
    try {
        const problems = await Problem.find({"status": req.query.status && req.query.status == 'completed' ? "completed" : "open"}).populate('attachments', {link: 1}).populate('resultAttachments', {link: 1}).sort({points: -1, impressions: 1}).limit(req.query.status && req.query.status == 'completed' ? 4 : 8);
        const count = await Problem.find({"status": req.query.status && req.query.status == 'completed' ? "completed" : "open"}).countDocuments();
        return res.json({problems: problems, count: count});
    } catch (err) {
        console.log(err);
        return res.status(500).json({success: false, error: err});
    }
});

router.get('/full', async (req, res) => {
    try {
        const problems = await Problem.find({"status": req.query.status && req.query.status == 'completed' ? "completed" : "open"}).populate('attachments', {link: 1}).populate('resultAttachments', {link: 1}).sort({points: -1, impressions: 1});
        const count = await Problem.find({"status": req.query.status && req.query.status == 'completed' ? "completed" : "open"}).countDocuments();
        return res.json({problems: problems, count: count});
    } catch (err) {
        console.log(err);
        return res.status(500).json({success: false, error: err});
    }
});

router.get('/account', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        const problems = await Problem.find({"author": req.user._id, "status": (req.query.status ? req.query.status : "open")}).populate('attachments', {link: 1}).populate('resultAttachments', {link: 1}).sort({points: -1, impressions: 1});
        
        return res.json({problems: problems});
    } catch (err) {
        console.log(err);
        return res.status(500).json({success: false, error: err});
    }
});

router.get('/:id', async (req, res) => {
    try {
        const problem = await Problem.findById(req.params.id).populate('attachments', {link: 1, mimetype: 1}).populate('resultAttachments', {link: 1, mimetype: 1});
        return res.json(problem);
    } catch (err) {
        console.log(err);
        return res.status(500).json({success: false, error: err});
    }
});


// функция конструирует запрос для Монгуса на поиск нескольких айдишников
function queryFewIds (a){
    a = a.map( a => a._id);
    return {'_id': { $in:a }};
}

router.post('/', passport.authenticate('jwt', {session: false}), upload.array('attachments'), /*validation.create,*/ async (req, res) => {
    try {
        const { description, address, coords } = req.body;
        const problem = new Problem({
            _id: new mongoose.Types.ObjectId(),
            author: req.user._id,
            address,
            location: (coords ? coords : []),
            ...(req.attachments && { attachments: req.attachments.map(a => a._id) }),
            description,
        });
        await problem.save();
        return res.json({success: true});
    } catch (err) {
        if (req.attachments)
            Attachment.removeWithFiles( queryFewIds( req.attachments ) );
        console.log(err);
        return res.status(500).json({success: false, error: err});
    }
});



router.post('/comment', async (req, res) => {
    try {
        
        return res.json({success: true});
    } catch (err) {
        if (req.attachments)
            Attachment.removeWithFiles( queryFewIds( req.attachments ) );
        console.log(err);
        return res.status(500).json({success: false, error: err});
    }
});

module.exports = router;