const socketioJwt = require('socketio-jwt');
const config = require('../config/config');

const User = require('../models/User.js');


module.exports = (server, app) => {
    const io = require('socket.io')(server);
    const clients = {};
    io.on('connection', socketioJwt.authorize({
        secret: config.secret,
    })).on('authenticated', client => {
        client.emit('authorized');

        if (!clients[client.decoded_token._id]) {
            clients[client.decoded_token._id] = [];
        }
        clients[client.decoded_token._id].push(client.id);

        client.on('disconnect', () => {
            if (clients[client.decoded_token._id].length > 1 && clients[client.decoded_token._id].indexOf(client.id) !== -1) {
                clients[client.decoded_token._id].splice(clients[client.decoded_token._id].indexOf(client.id), 1);
                return;
            }
            delete clients[client.decoded_token._id];
        });
    });
};