module.exports = {
    'secret': process.env.SECRET_KEY,
    'tokenExpiresIn': process.env.TOKEN_EXPIRES_IN,
    'frontendUrl': process.env.FRONTEND_URI,
    'backendUrl': process.env.BACKEND_URI,
    'database': process.env.DATABASE
};